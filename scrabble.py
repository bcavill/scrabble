from flask import Flask, render_template, url_for, request, redirect
import functions

def index():
    scores = functions.do_query("""SELECT AVG(score) as score, members.name AS name, gameid, games.datetime AS datetime FROM scores JOIN games ON scores.gameid=games.id JOIN members ON scores.player=members.id GROUP BY members.id HAVING count(gameid) >= 10 ORDER BY score DESC limit 10""", dict=True)

    return render_template('index.html', scores=scores)

def members():
    members = functions.do_query("""SELECT id, name, address FROM members""", dict=True)

    return render_template('members.html', members=members)

def member():
    member_id = request.form['id']
    member = functions.do_query("""SELECT name, AVG(score) as average FROM members JOIN scores on scores.player=members.id WHERE members.id=%s GROUP BY members.id""", (member_id,), dict=True)[0]

    wins = functions.do_query("""SELECT count(win) FROM scores WHERE win = 1 AND player=%s""", (member_id,))[0][0]
    losses = functions.do_query("""SELECT count(win) FROM scores WHERE win = 0 AND player=%s""", (member_id,))[0][0]
    highest_score = functions.do_query("""SELECT gameid, max(score) AS score, datetime, location FROM scores join games on scores.gameid = games.id WHERE player=%s""", (member_id,), dict=True)[0]
    highest_score_opponent = functions.do_query("""SELECT name FROM scores join members on members.id = scores.player WHERE gameid = %s AND player != %s""", (highest_score['gameid'], member_id))[0][0]

    return render_template('member.html', member=member, wins=wins, losses=losses, highest_score=highest_score, highest_score_opponent=highest_score_opponent, member_id = member_id)

def create_member():
    member_id = functions.do_query("""INSERT INTO members (name, `mobile number`, address, postcode) VALUES (%s, %s, %s, %s)""", (request.form['name'], request.form['mobile'], request.form['address'], request.form['postcode']), commit=True)
    return redirect(url_for('members'))

def edit_member():
    member_id = request.form['member_id']
    member = functions.do_query("""SELECT name, `mobile number`, address, postcode FROM members WHERE id=%s""", (member_id,), dict=True)[0]

    return render_template('edit_member.html', member = member, member_id = member_id)

def save_member_changes():
    member_id = request.form['member_id']
    functions.do_query("""UPDATE members SET name=%s, `mobile number`=%s, address=%s, postcode=%s WHERE id=%s""", (request.form['name'], request.form['mobile'], request.form['address'], request.form['postcode'], member_id), commit=True)

    return redirect(url_for('members'))