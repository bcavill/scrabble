from flask import Flask, render_template
application = Flask(__name__)

import os, sys
sys.path.insert(0, os.path.dirname(__file__))

@application.route('/')
def index():
    import scrabble
    return scrabble.index()

@application.route('/members')
def members():
    import scrabble
    return scrabble.members()

@application.route('/member', methods=('post',))
def member():
    import scrabble
    return scrabble.member()

@application.route('/new_member')
def new_member():
    return render_template('new_member.html')

@application.route('/create_member', methods=('post',))
def create_member():
    import scrabble
    return scrabble.create_member()

@application.route('/edit_member', methods=('post',))
def edit_member():
    import scrabble
    return scrabble.edit_member()

@application.route('/save_member_changes', methods=('post',))
def save_member_changes():
    import scrabble
    return scrabble.save_member_changes()