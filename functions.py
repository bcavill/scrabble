import configparser
import MySQLdb

config = configparser.ConfigParser()
config.read('/var/www/scrabble/settings.conf')

def do_query(query, args = None, dict = False, commit = False):
    db = MySQLdb.connect(host = config['db']['host'], user = config['db']['user'], passwd = config['db']['passwd'], db = config['db']['database'])
    cur = db.cursor()
    if dict:
        cur = db.cursor(MySQLdb.cursors.DictCursor)

    cur.execute(query, args)

    if commit:
        db.commit()

    return cur.fetchall()